/* Copyright 1996 Acorn Computers Ltd
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
/*
 * panes.c
 * Handle paned windows.
 */

#include "swis.h"
#include "main.h"
#include "panes.h"
#include "fonts.h"
#include "icons.h"

int opencount = 0;


/* Pane data */

TemplateRec templates [] =
{
    {"Download", NULL, NULL, 0},
    {"List", NULL, NULL, 0},
    {"ProgInfo", NULL, NULL, 0},
};

/* Storage for templates */


int sizex, sizey;
int offx, offy;
int scrollx, scrolly;
int extent_minx, extent_miny, extent_maxx, extent_maxy;

void load_template (int num)
{
    int buflen, indirlen;
    char *wbuf, *ibuf;

    _swix(Wimp_LoadTemplate, _IN(1) | _INR(4,6) | _OUTR(1,2), -1, -1, (int) templates[num].name, 0, &buflen, &indirlen);

    wbuf = calloc(1, buflen + 4);
    ibuf = calloc(1, indirlen);

    _swix(Wimp_LoadTemplate, _INR(1,6), (int) wbuf + 4, (int) ibuf, (int) ibuf + indirlen, -1, (int) templates[num].name, 0);

    templates[num].wbuf = wbuf;
    templates[num].ibuf = ibuf;

#if 0
    {   char err[100];
        sprintf(err, "abcdIndir is %d and main is %d\n", indirlen, buflen);
        _swix(Wimp_ReportError, _INR(0,2), err, 0, templates[num].name);
    }
#endif
}

void create_window (int num)
{
    int *wbuf = (int *) templates[num].wbuf;
    _swix(Wimp_CreateWindow, _IN(1) | _OUT(0), (int) (wbuf + 1), wbuf);
}

void open_window (int num, int behind)
{
    int *wbuf = (int *) templates[num].wbuf;
    wbuf[7] = behind;
    _swix(Wimp_OpenWindow, _IN(1), (int) wbuf);
}

/*
 * User requests the windows to be opened.  If they're
 * already open, bring them to the top.
 */
void openwins (void)
{
    opencount = 1;
#if 0
    open_window(PANE_LIST, -1);                    /* list window at top of stack */
    open_window(PANE_MAIN, handle_of(PANE_LIST));  /* parent window behind it */
#endif

    open_window(PANE_MAIN, -1);                    /* parent window at top of stack */

    _swix(Wimp_GetWindowState, _IN(1), (int) templates[PANE_MAIN].wbuf);
    minx(PANE_LIST) = minx(PANE_MAIN) + offx;
    miny(PANE_LIST) = miny(PANE_MAIN) + offy;
    maxx(PANE_LIST) = minx(PANE_LIST) + sizex;
    maxy(PANE_LIST) = miny(PANE_LIST) + sizey;

    open_window(PANE_LIST, -1);                    /* list window at top of stack */
}

/*
 * Wimp requests windows to be (re)opened
 */

void reopenwins (int *buf, Bool modech)
{
    int behindthis = buf[7];
    if (buf[0] == handle_of(PANE_LIST))
    {
        scrollx(PANE_LIST) = buf[5];
        scrolly(PANE_LIST) = buf[6];
        _swix(Wimp_OpenWindow, _IN(1), (int) buf);
    } else if (buf[0] == handle_of(PANE_MAIN))
    {
        if (modech)
        {
            /* Open the main window first */
            memcpy((void *)(templates[PANE_MAIN].wbuf + 4), (void *)(buf + 1), 24);
            open_window(PANE_MAIN, behindthis);
            /* Find out where it really opened */
            _swix(Wimp_GetWindowState, _IN(1), (int) templates[PANE_MAIN].wbuf);
            minx(PANE_LIST) = minx(PANE_MAIN) + offx;
            miny(PANE_LIST) = miny(PANE_MAIN) + offy;
            maxx(PANE_LIST) = minx(PANE_LIST) + sizex;
            maxy(PANE_LIST) = miny(PANE_LIST) + sizey;
            open_window(PANE_LIST, behindthis);
            open_window(PANE_MAIN, handle_of(PANE_LIST));
        } else
        {
            /* First open the list window */
            minx(PANE_LIST) = buf[1] + offx;
            miny(PANE_LIST) = buf[2] + offy;
            maxx(PANE_LIST) = minx(PANE_LIST) + sizex;
            maxy(PANE_LIST) = miny(PANE_LIST) + sizey;
            open_window(PANE_LIST, behindthis);
            /* Now open the main window behind it */
            memcpy((void *)(templates[PANE_MAIN].wbuf + 4), (void *)(buf + 1), 24);
            open_window(PANE_MAIN, handle_of(PANE_LIST));
        }
    } else if (buf[0] == handle_of(PANE_INFO))
        _swix(Wimp_OpenWindow, _IN(1), (int) buf);

}

void closewins (void)
{
    if (opencount == 0)
        return;
    opencount = 0;
    _swix(Wimp_CloseWindow, _IN(1), (int) templates[PANE_MAIN].wbuf);
    _swix(Wimp_CloseWindow, _IN(1), (int) templates[PANE_LIST].wbuf);
}


void find_offsets (void)
{
    /* Find offsets from parent window to child window */
    offx  = minx(PANE_LIST) - minx(PANE_MAIN);
    offy  = miny(PANE_LIST) - miny(PANE_MAIN);
    sizex = maxx(PANE_LIST) - minx(PANE_LIST);
    sizey = maxy(PANE_LIST) - miny(PANE_LIST);
}

/*
 * Recalculate work area extent.  The origin 0, 0 will be at the top left.
 * The y-coordinates are therefore negative.  Ensure that the total size
 * is at least sizex, sizey.
 */

void fix_extent (Bool scroll_to_bottom)
{
    int n = num_fonts(0);
    int extent = ICON_MARGIN + n * icon_distance;
    int blk [4];
    if (extent < sizey)
        extent = sizey;
    extent_minx = blk[0] = 0;
    extent_miny = blk[1] = -extent;
    extent_maxx = blk[2] = sizex;
    extent_maxy = blk[3] = 0;
    _swix(Wimp_SetExtent, _INR(0,1), handle_of(PANE_LIST), (int) blk);

    /* Ensure scrollbar offsets are sensible */
    _swix(Wimp_GetWindowState, _IN(1), templates[PANE_LIST].wbuf);

    if (scroll_to_bottom || extent + scrolly(PANE_LIST) < sizey)
    {
        scrolly(PANE_LIST) = sizey - extent;
        if (opencount > 0) _swix(Wimp_OpenWindow, _IN(1), (int) templates[PANE_LIST].wbuf);
    }
}

void redraw_win (int *buf)
{
    int more;

    _swix(Wimp_RedrawWindow, _IN(1) | _OUT(0), (int) buf, &more);
    while (more != 0)
    {
/*
 * Work area coords to be redrawn:-
 */
        int adjusty = buf[4] - buf[6];
        int workminy = buf[8] - adjusty;
        int workmaxy = buf[10] - adjusty;

        FontPtr font;
        int ymin = extent_maxy - ICON_MARGIN - icon_distance;
        for (font = firstFont; font; font = font->next)
        {
/*
 * Does any part of the icon lie in the redraw area?
 */
             int ymax = ymin + icon_distance;
             if (ymin <= workmaxy && ymax >= workminy)
             {
                 set_icon_to(font, ymin);
                 plot_icon();
             }
             ymin -= icon_distance;
        }
/*
 * Is there another rectangle?
 */
        _swix(Wimp_GetRectangle, _IN(1) | _OUT(0), (int) buf, &more);
    }
}

/*
 * Update whole list window (visible part)
 */

void update_win (void)
{
    int wminx, wminy, wmaxx, wmaxy;
    /* Get the visible part of the work area */
    wminx = scrollx(PANE_LIST);
    wmaxy = scrolly(PANE_LIST);
    wmaxx = wminx + (maxx(PANE_LIST) - minx(PANE_LIST));
    wminy = wmaxy - (maxy(PANE_LIST) - miny(PANE_LIST));
    update_partial_win(wminx, wminy, wmaxx, wmaxy);
}


/*
 * Force redraw of a whole window (visible part).  This is
 * different from update_win in that it clears the area
 * first, and is useful when stuff has been deleted.
 * Pass title_too == TRUE to update the title bar as well.
 */

void force_redraw_win (int pane)
{
    int wminx, wminy, wmaxx, wmaxy;
    /* Get the visible part of the work area */
    wminx = scrollx(pane);
    wmaxy = scrolly(pane);
    wmaxx = wminx + (maxx(pane) - minx(pane));
    wminy = wmaxy - (maxy(pane) - miny(pane));
    _swix(Wimp_ForceRedraw, _INR(0,4), handle_of(pane), wminx, wminy, wmaxx, wmaxy);
}


/*
 * Redraw the title bar of a given window
 */

void force_redraw_titlebar (int pane)
{
    _swix(Wimp_ForceRedraw, _INR(0,4), -1, minx(pane), maxy(pane), maxx(pane), maxy(pane) + 40);
}


/*
 * Update the specifed portion of the list window
 */

void update_partial_win (int wminx, int wminy, int wmaxx, int wmaxy)
{
    int buf[11], more;
    if (firstFont == NULL)
        return;
    buf[0] = handle_of(PANE_LIST);
    buf[1] = wminx;
    buf[2] = wminy;
    buf[3] = wmaxx;
    buf[4] = wmaxy;

    _swix(Wimp_UpdateWindow, _IN(1) | _OUT(0), (int) buf, &more);
    while (more != 0)
    {
/*
 * Work area coords to be redrawn:-
 */
        int adjusty = buf[4] - buf[6];
        int workminy = buf[8] - adjusty;
        int workmaxy = buf[10] - adjusty;

        FontPtr font;
        int ymin = extent_maxy - ICON_MARGIN - icon_distance;
        for (font = firstFont; font; font = font->next)
        {
/*
 * Does any part of the icon lie in the redraw area?
 */
             int ymax = ymin + icon_distance;
             if (ymin <= workmaxy && ymax >= workminy)
             {
                 set_icon_to(font, ymin);
                 plot_icon();
             }
             ymin -= icon_distance;
        }
/*
 * Is there another rectangle?
 */
        _swix(Wimp_GetRectangle, _IN(1) | _OUT(0), (int) buf, &more);
    }
}



/*
 * Ensure that the region between the given y-coordinates (which are work-area
 * relative) are visible, scrolling if necessary.
 */

void ensure_visible (int minpos, int maxpos)
{
    _swix(Wimp_GetWindowState, _IN(1), templates[PANE_LIST].wbuf);
    {
        int winheight = maxy(PANE_LIST) - miny(PANE_LIST);
        int sy = scrolly(PANE_LIST);

        if (minpos < sy - winheight)
            sy = minpos + winheight;
        else if (maxpos > sy)
            sy = maxpos;
        else
            return;
        scrolly(PANE_LIST) = sy;
        _swix(Wimp_OpenWindow, _IN(1), (int) templates[PANE_LIST].wbuf);
    }
}


/*
 * Set the title area of the given window to a static buffer.
 * Does not cause an update.  The title icon MUST be indirected text.
 */

void set_title_memory (int pane, char *buffer, int length)
{
    tdata1(pane) = (int) buffer;
    tdata3(pane) = length;
}


/*
 * Set the text buffer for the 'icon'th icon in the given pane.  Icons
 * are numbered from 0.  The icon MUST be indirected text.
 * Does not cause an update.
 */

void set_icon_memory(int pane, int icon, char *buffer, int length)
{
    int *icondef = ((int *) templates[pane].wbuf) + 23 + 8 * icon + 5;

    icondef[0] = (int) buffer;
    icondef[2] = length;
}


void change_icon_flags (int pane, int icon, unsigned int clear, unsigned int eor)
{
    int blk[4];
    blk[0] = handle_of(pane);
    blk[1] = icon;
    blk[2] = eor;
    blk[3] = clear;
    _swix(Wimp_SetIconState, _IN(1), blk);
}
